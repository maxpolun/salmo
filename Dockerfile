FROM rust:1.64.0 as build

WORKDIR /usr/src/salmo

ADD Cargo.toml Cargo.lock ./
ADD src/ src/

RUN cargo install --path .

FROM debian:bullseye-slim

COPY --from=build /usr/local/cargo/bin/salmo /usr/local/bin/salmo

CMD ["salmo"]
