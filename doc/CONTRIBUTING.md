# Contributing

You need to install rust and cargo, the simplest way is via [rustup](https://rustup.rs/)

You should be able to build a local copy by `cargo build`.

Run tests via `cargo test`
