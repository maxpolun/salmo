# Roadmap

A list of desired features/ideas for future development

## Before 1.0

* Supporting other databases (at least mysql and sqlite)
* Generate revert migrations (create a new migration that reverts a particular migration)
* Comprehensive tests (both unit and e2e)
* Clean API/CLI seperation (API is usable)
* Better CLI display (especially for status)

## Other ideas

* Auto-revert: Infer how to revert changes for uncommitted migrations, so that revert scripts are optional
* Per-migration meta.toml: specify some additional metadata for migrations in a config file
* Background migrations: For migrations that would take a long time:
  * Run a prepare migration (e.g. create an optional new column)
  * Continuously run a migration script until some criteria (e.g. move data from the old column to the new until no rows contain the old column)
  * Run a completion migration (drop the old column and make the new one required)
  * All of this is probably specified in a meta.toml for the migration
