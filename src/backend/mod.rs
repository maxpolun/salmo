use crate::migration_data::{committed::CommittedFile, migrations::Migration};

#[cfg(feature = "db_postgresql")]
pub mod pg;

#[derive(Debug, Hash, PartialEq, Eq, Clone, Copy)]
pub enum MigrationStatus {
    Untried,
    Tried { up_to_date: bool },
    Committed { tried: bool },
    Executed,
}

#[derive(Debug, Hash, PartialEq, Eq, Clone)]
pub struct MigrationWithStatus {
    pub migration: Migration,
    pub status: MigrationStatus,
}

pub trait DatabaseBackend {
    fn migration_status(
        &mut self,
        commits: &CommittedFile,
        migrations: &[Migration],
    ) -> anyhow::Result<Vec<MigrationWithStatus>>;
    fn try_migrations(&mut self, migration: &[MigrationWithStatus]) -> anyhow::Result<()>;
    fn untry_migrations(
        &mut self,
        migration: &[MigrationWithStatus],
        execute_revert_script: bool,
    ) -> anyhow::Result<()>;
    fn execute_migrations(&mut self, migration: &[MigrationWithStatus]) -> anyhow::Result<()>;

    fn transaction<'a>(&'a mut self) -> anyhow::Result<Box<dyn BackendTransaction + 'a>>;
}

pub trait BackendTransaction {
    fn commit(&mut self) -> anyhow::Result<()>;
    // drop is rollback

    fn execute(&mut self, migration: &Migration) -> anyhow::Result<()>;
    fn revert(&mut self, migration: &Migration) -> anyhow::Result<()>;
    fn mark_executed(&mut self, migration: &Migration) -> anyhow::Result<()>;

    fn mark_tried(&mut self, migration: &Migration) -> anyhow::Result<()>;
    fn mark_untried(&mut self, migration: &Migration) -> anyhow::Result<()>;
}
