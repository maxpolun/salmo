use std::{
    env::{self, current_dir},
    fs,
};

use anyhow::bail;
use clap::{builder::TypedValueParser, Arg, Args, Parser, Subcommand};
use libsalmo::{
    backend::{MigrationStatus, MigrationWithStatus},
    config::{config_file_exists_in_dir, get_config},
    salmo_contex::SalmoContext,
    workflows::{
        commit::commit_migration, migrate::run_migrations, status::get_status,
        tries::reset_all_tries,
    },
};
use log::{debug, LevelFilter};
use simplelog::{ColorChoice, TermLogger, TerminalMode};

#[derive(Copy, Clone, Debug)]
#[non_exhaustive]
pub struct MigrationIdValueParser {}

impl MigrationIdValueParser {
    /// Parse non-empty string values
    pub fn new() -> Self {
        Self {}
    }
}

impl TypedValueParser for MigrationIdValueParser {
    type Value = String;

    fn parse_ref(
        &self,
        _cmd: &clap::Command,
        _arg: Option<&Arg>,
        value: &std::ffi::OsStr,
    ) -> Result<Self::Value, clap::Error> {
        let value = value.to_str().ok_or_else(|| {
            clap::Error::raw(clap::error::ErrorKind::InvalidUtf8, "error parsing string")
        })?;
        if value.is_empty()
            || !value
                .chars()
                .all(|c| c.is_ascii_alphanumeric() || c == '_' || c == '-')
        {
            return Err(clap::Error::raw(clap::error::ErrorKind::ValueValidation, "invalid migration identifier, must be ascii alphanumeric with underscores or dashes"));
        }
        Ok(value.to_owned())
    }
}

impl Default for MigrationIdValueParser {
    fn default() -> Self {
        Self::new()
    }
}

#[derive(Debug, Parser)]
#[clap(author, version, about, long_about = None)]
struct CliArgs {
    /// log verbosely
    #[clap(short, long, action, default_value_t = false)]
    verbose: bool,

    /// do not print normal output
    #[clap(short, long, action, default_value_t = false)]
    quiet: bool,

    /// Which environments are considered to be active currently
    #[clap(short, long, value_parser = MigrationIdValueParser::new(), require_equals = true, use_value_delimiter = true, env = "SALMO_ENVS")]
    environments: Option<Vec<String>>,

    #[clap(subcommand)]
    command: Command,
}

#[derive(Debug, Subcommand)]
enum Command {
    /// initialize a new project
    Init,
    /// create a new migration
    New(MigrationId),
    /// execute any pending committed, but not yet executed, migrations
    Migrate,
    /// see the status of migrations in your project
    Status(StatusFlags),
    /// revert any tried, but not committed, migrations
    Reset,
    /// commit a tried migration
    Commit(MigrationId),
    /// execute a migration locally, without committing
    Try(MigrationId),
    /// revert and re-execute a currently tried migration
    Retry(MigrationId),
    /// revert a currently tried migration
    Untry(MigrationId),
}

#[derive(Debug, Args)]
struct MigrationId {
    /// The migration id
    #[clap(value_parser = MigrationIdValueParser::new())]
    migration_id: String,
}

#[derive(Debug, Args)]
struct StatusFlags {
    /// Show executed, committed migrations
    #[arg(long, default_value_t = false)]
    show_executed: bool,
    /// Show committed migrations that have not been executed
    #[arg(long, default_value_t = true)]
    show_pending: bool,
    /// Show currently being tried, non-committed migrations
    #[arg(long, default_value_t = true)]
    show_tried: bool,
    /// Show written, but not tried, not committed migrations
    #[arg(long, default_value_t = true)]
    show_untried: bool,
}

fn run() -> Result<(), anyhow::Error> {
    let args = CliArgs::parse();
    let mut level = match (args.quiet, args.verbose) {
        (true, true) => bail!("error: cannot be both quiet and verbose"),
        (true, false) => LevelFilter::Error,
        (false, true) => LevelFilter::Info,
        (false, false) => LevelFilter::Warn,
    };
    if let Ok(text) = env::var("SALMO_DEBUG") {
        if text == "1" || text == "true" {
            level = LevelFilter::Debug
        }
    }
    if let Ok(text) = env::var("SALMO_TRACE") {
        if text == "1" || text == "true" {
            level = LevelFilter::Trace
        }
    }
    TermLogger::init(
        level,
        simplelog::Config::default(),
        TerminalMode::Mixed,
        ColorChoice::Auto,
    )?;
    match args.command {
        Command::Init => {
            if config_file_exists_in_dir(&mut current_dir()?) {
                println!("Salmo.toml already exists in this directory");
            } else {
                fs::write(
                    current_dir()?.join("Salmo.toml"),
                    include_bytes!("default_config.toml"),
                )?;
                fs::create_dir(current_dir()?.join("migrations"))?;
                println!("Salmo.toml created");
            }
        }
        Command::Status(ref flags) => {
            let status = get_status(get_ctx(&args)?)?;

            for (env_name, summary) in status {
                println!("For environment `{}`", env_name);
                if flags.show_executed && !summary.already_migrated.is_empty() {
                    println!("Already migrated:");
                    for m in summary.already_migrated {
                        println!("\t{}", m)
                    }
                    println!()
                }

                if flags.show_pending && !summary.pending_migration.is_empty() {
                    println!("Pending migration:");
                    for m in summary.pending_migration {
                        println!("\t{}", m)
                    }
                    println!()
                }

                if flags.show_tried && !summary.tries.is_empty() {
                    println!("Currently being tried:");
                    for status in summary.tries {
                        println!(
                            "\t{} {}",
                            status.id,
                            if status.up_to_date {
                                ""
                            } else {
                                "[need retry]"
                            }
                        )
                    }
                }

                if flags.show_untried && !summary.untried.is_empty() {
                    println!("Not yet tried:");
                    for m in summary.untried {
                        println!("\t{}", m)
                    }
                    println!()
                }
            }
        }
        Command::New(ref m) => {
            let ctx = get_ctx(&args)?;
            let migration_dir = ctx.config.migrations_directory.join(&m.migration_id);
            fs::create_dir(&migration_dir)?;
            fs::write(migration_dir.join("migrate.sql"), "-- This is your migration file: change your database in development and production")?;
            fs::write(
                migration_dir.join("revert.sql"),
                "-- This is your revert file: undo the migration changes in local development",
            )?;
        }
        Command::Migrate => {
            let ctx = get_ctx(&args)?;
            run_migrations(&ctx)?;
        }
        Command::Commit(ref m) => {
            let ctx = get_ctx(&args)?;
            let commit = commit_migration(&ctx, &m.migration_id)?;
            println!("COMITTED {} {}", commit.id, commit.hash);
        }
        Command::Reset => {
            let ctx = get_ctx(&args)?;
            let tries = reset_all_tries(&ctx)?;
            for (env, tries) in tries.iter() {
                println!("FOR ENV: {}", env);
                for tried in tries.iter() {
                    println!("REVERT {}", tried.migration.id);
                }
            }
        }
        Command::Try(ref m) => {
            let ctx = get_ctx(&args)?;
            let migrations = ctx.migrations()?;
            let commits = ctx.commits()?;
            let migration = if let Some(m) = migrations.db.get(&m.migration_id) {
                m
            } else {
                bail!("migration id `{}` not found", m.migration_id)
            };
            let hash = migration.migrate_hash()?;
            for env in ctx.environments {
                let mut backend = env.backend()?;
                let ms = backend
                    .migration_status(&commits, &[migration.clone()])?
                    .into_iter()
                    .next()
                    .unwrap();
                match ms.status {
                    MigrationStatus::Untried => {
                        backend.try_migrations(&[ms])?;
                        println!("[{}] TRY {}: {}", env.name, migration.id, hash);
                    }
                    MigrationStatus::Tried { up_to_date } => {
                        if up_to_date {
                            debug!("[{}] skipping {}, up to date", env.name, migration.id)
                        } else {
                            println!("[{}] {}: needs retry", env.name, migration.id);
                        }
                    }
                    MigrationStatus::Committed { tried: _ } => {
                        bail!("migration {} already committed", migration.id)
                    }
                    MigrationStatus::Executed => {
                        bail!("migration {} already committed and executed", migration.id)
                    }
                }
            }
        }
        Command::Untry(ref m) => {
            let ctx = get_ctx(&args)?;
            let migrations = ctx.migrations()?;
            let migration = &migrations.db[&m.migration_id];
            let hash = migration.migrate_hash()?;
            let commits = ctx.commits()?;
            for env in ctx.environments {
                let mut backend = env.backend()?;
                let ms = backend
                    .migration_status(&commits, &[migration.clone()])?
                    .into_iter()
                    .next()
                    .unwrap();
                match ms.status {
                    MigrationStatus::Untried => {
                        println!("[{}] `{}` was not tried yet", env.name, migration.id);
                    }
                    MigrationStatus::Tried { up_to_date: _ } => {
                        backend.untry_migrations(&[ms], true)?;
                        println!("[{}] UNTRY {}: {}", env.name, migration.id, hash);
                    }
                    MigrationStatus::Committed { tried: _ } => {
                        bail!("migration {} already committed", migration.id)
                    }
                    MigrationStatus::Executed => {
                        bail!("migration {} already committed and executed", migration.id)
                    }
                }
            }
        }
        Command::Retry(ref m) => {
            let ctx = get_ctx(&args)?;
            let migrations = ctx.migrations()?;
            let migration = &migrations.db[&m.migration_id];
            let hash = migration.migrate_hash()?;
            let commits = ctx.commits()?;
            for env in ctx.environments {
                let mut backend = env.backend()?;
                let ms = backend
                    .migration_status(&commits, &[migration.clone()])?
                    .into_iter()
                    .next()
                    .unwrap();
                match ms.status {
                    MigrationStatus::Untried => {
                        backend.try_migrations(&[ms])?;
                        println!("[{}] TRY {}: {}", env.name, migration.id, hash);
                    }
                    MigrationStatus::Tried { up_to_date: _ } => {
                        backend.untry_migrations(&[ms.clone()], true)?;
                        backend.try_migrations(&[MigrationWithStatus {
                            migration: ms.migration,
                            status: MigrationStatus::Untried,
                        }])?;
                        println!("[{}] RETRY {}: {}", env.name, migration.id, hash);
                    }
                    MigrationStatus::Committed { tried: _ } => {
                        bail!("migration {} already committed", migration.id)
                    }
                    MigrationStatus::Executed => {
                        bail!("migration {} already committed and executed", migration.id)
                    }
                }
            }
        }
    };
    Ok(())
}

fn get_ctx(args: &CliArgs) -> anyhow::Result<SalmoContext> {
    let config = get_config()?;
    let env_names = args
        .environments
        .as_ref()
        .unwrap_or(&config.default_environments);
    let mut envs = Vec::new();
    for name in env_names {
        envs.push(
            config
                .environments
                .iter()
                .find(|e| &e.name == name)
                .ok_or_else(|| anyhow::anyhow!("unrecognized environment: {}", name))?
                .clone(),
        )
    }
    Ok(SalmoContext {
        config,
        environments: envs,
    })
}

fn main() {
    match run() {
        Ok(_) => {} // do nothing
        Err(e) => {
            println!("Error encountered: {}", e)
        }
    }
}
