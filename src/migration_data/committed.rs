use std::{
    collections::HashMap,
    fs::{self, read_to_string},
    io::ErrorKind,
    path::Path,
};

use anyhow::{anyhow, Context};
use log::{info, trace};
use serde::{Deserialize, Serialize};
use toml_edit::{array, value, Document, Table};

use crate::salmo_contex::SalmoContext;

use super::migrations::Migration;

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct CommittedFile {
    pub commits: Vec<Commit>,
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct Commit {
    pub id: String,
    /// the hex encoded string of the hash
    pub hash: String,
}

impl CommittedFile {
    pub fn load(dir: &Path) -> anyhow::Result<Self> {
        trace!("loading committed file at {:?}", dir);
        let file = match read_to_string(dir.join("committed.toml")) {
            Ok(s) => s,
            Err(e) => match e.kind() {
                ErrorKind::NotFound => {
                    info!("No committed file. Assuming no commits, this is fine.");
                    return Ok(CommittedFile {
                        commits: Vec::new(),
                    });
                }
                _ => return Err(e.into()),
            },
        };

        toml_edit::de::from_str(&file).context("parsing committed.toml")
    }

    pub fn add_commit(ctx: &SalmoContext, migration_id: &str) -> anyhow::Result<Commit> {
        let dir = ctx.config.migrations_directory.join("committed.toml");
        let file = read_to_string(&dir).or_else(|e| match e.kind() {
            ErrorKind::NotFound => Ok("".to_string()),
            _ => Err(e),
        })?;
        let mut doc = file.parse::<Document>()?;
        doc.entry("version").or_insert(value("1"));
        let commits = doc
            .entry("commits")
            .or_insert(array())
            .as_array_of_tables_mut()
            .ok_or_else(|| {
                anyhow!("corrupt committed.toml: the [commits] value must be an array of tables")
            })?;

        let migrations = ctx.migrations()?;
        let m = migrations
            .db
            .get(migration_id)
            .ok_or_else(|| anyhow!("migration with id `{}` does not exist!", migration_id))?;
        let hash = m.migrate_hash()?;

        let mut commit_table = Table::new();
        commit_table.insert("id", value(migration_id));
        commit_table.insert("hash", value(&hash));

        commits.push(commit_table);
        fs::write(dir, doc.to_string())?;

        Ok(Commit {
            id: migration_id.to_string(),
            hash,
        })
    }

    pub fn commits_hash(&self) -> HashMap<String, Commit> {
        self.commits
            .iter()
            .map(|c| (c.id.clone(), c.clone()))
            .collect()
    }

    pub fn contains_migration(&self, m: &Migration) -> bool {
        self.commits.iter().any(|c| c.id == m.id)
    }
}
