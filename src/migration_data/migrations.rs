use std::{
    cell::OnceCell,
    collections::HashMap,
    fs::read_to_string,
    hash::Hash,
    io::Error,
    path::{Path, PathBuf},
};

use log::trace;
use sha2::{Digest, Sha256};

#[derive(Debug, Clone, Eq)]
pub struct Migration {
    pub id: String,
    pub path: PathBuf,
    // cache fields
    msql: OnceCell<String>,
    rsql: OnceCell<String>,
}

impl PartialEq for Migration {
    fn eq(&self, other: &Self) -> bool {
        self.id == other.id && self.path == other.path
    }
}

impl Hash for Migration {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        self.id.hash(state);
        self.path.hash(state);
    }
}

impl Migration {
    pub fn new(id: String, path: PathBuf) -> Self {
        Self {
            id,
            path,
            msql: OnceCell::new(),
            rsql: OnceCell::new(),
        }
    }

    pub fn migrate_path(&self) -> PathBuf {
        self.path.join("migrate.sql")
    }

    pub fn revert_path(&self) -> PathBuf {
        self.path.join("revert.sql")
    }

    pub fn migrate_sql(&self) -> anyhow::Result<&String> {
        if let Some(sql) = self.msql.get() {
            Ok(sql)
        } else {
            let sql = read_to_string(self.migrate_path())?;
            self.msql.set(sql).unwrap();
            Ok(self.msql.get().unwrap())
        }
    }

    pub fn revert_sql(&self) -> anyhow::Result<&String> {
        if let Some(sql) = self.rsql.get() {
            Ok(sql)
        } else {
            let sql = read_to_string(self.revert_path())?;
            self.rsql.set(sql).unwrap();
            Ok(self.rsql.get().unwrap())
        }
    }

    pub fn migrate_hash(&self) -> anyhow::Result<String> {
        let contents = self.migrate_sql()?;
        let mut hasher = Sha256::new();
        hasher.update(contents);
        Ok(hex::encode(hasher.finalize()))
    }
}

pub struct MigrationRegistry {
    pub db: HashMap<String, Migration>,
}

impl MigrationRegistry {
    pub fn load(dir: &Path) -> Result<Self, Error> {
        trace!("loading registry of migrations in dir {:?}", dir);
        let db: HashMap<String, Migration> = dir
            .read_dir()?
            .filter_map(|entry| {
                let entry = entry.ok()?;
                if entry.file_type().ok()?.is_dir() {
                    let id = entry.path().file_name()?.to_str()?.to_owned();
                    Some((id.clone(), Migration::new(id, entry.path())))
                } else {
                    None
                }
            })
            .collect();
        Ok(Self { db })
    }
}
