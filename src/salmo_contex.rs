use crate::{
    config::{Config, ConfigEnvironment},
    migration_data::{committed::CommittedFile, migrations::MigrationRegistry},
};

pub struct SalmoContext {
    pub config: Config,
    pub environments: Vec<ConfigEnvironment>,
}

impl SalmoContext {
    pub fn commits(&self) -> anyhow::Result<CommittedFile> {
        CommittedFile::load(&self.config.migrations_directory)
    }

    pub fn migrations(&self) -> anyhow::Result<MigrationRegistry> {
        Ok(MigrationRegistry::load(&self.config.migrations_directory)?)
    }
}
