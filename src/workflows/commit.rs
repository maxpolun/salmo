use crate::{
    backend::{MigrationStatus, MigrationWithStatus},
    migration_data::committed::{Commit, CommittedFile},
    salmo_contex::SalmoContext,
};
use anyhow::anyhow;

pub fn commit_migration(ctx: &SalmoContext, migration_id: &str) -> anyhow::Result<Commit> {
    let migrations = ctx.migrations()?;
    let migration = migrations.db[migration_id].clone();
    let commits = ctx.commits()?;
    let mut backends = ctx
        .environments
        .iter()
        .map(|e| e.backend())
        .collect::<anyhow::Result<Vec<_>>>()?;
    let status = backends
        .iter_mut()
        .map(|b| {
            Ok(b.migration_status(&commits, &[migration.clone()])?
                .into_iter()
                .next()
                .unwrap())
        })
        .collect::<anyhow::Result<Vec<MigrationWithStatus>>>()?;

    if !status
        .iter()
        .all(|m| matches!(m.status, MigrationStatus::Tried { up_to_date: _ }))
    {
        return Err(anyhow!(
            "Migration `{}` has not been tried in an environment. Commit aborted.",
            migration_id
        ));
    }

    if !status.iter().all(|m| match m.status {
        MigrationStatus::Tried { up_to_date } => up_to_date,
        _ => false,
    }) {
        return Err(anyhow!("Migration `{}` was changed after last being tried. Retry it in order to commit. Commit aborted.", migration_id));
    }

    // let's be careful how we do this:
    // 1. start transactions in all backends
    // 2. update the database fields
    // 3. update the file
    // 4. finally commit
    // I think this means that the only way the commit file and database can get out of step is if the final db commit fails.
    // which should be pretty rare.
    let mut ts = backends
        .iter_mut()
        .map(|b| b.transaction())
        .collect::<anyhow::Result<Vec<_>>>()?;

    for t in ts.iter_mut() {
        t.mark_untried(&migration)?;
        t.mark_executed(&migration)?;
    }

    let commit = CommittedFile::add_commit(ctx, migration_id)?;

    for mut t in ts {
        t.commit()?;
    }

    Ok(commit)
}
