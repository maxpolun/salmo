use crate::{backend::MigrationStatus, salmo_contex::SalmoContext};

pub fn run_migrations(ctx: &SalmoContext) -> anyhow::Result<()> {
    let commits = ctx.commits()?;
    let migrations_registry = ctx.migrations()?;
    let committed_migrations = commits
        .commits
        .iter()
        .map(|c| migrations_registry.db[&c.id].clone())
        .collect::<Vec<_>>();
    for env in ctx.environments.iter() {
        let mut backend = env.backend()?;
        let to_be_executed = backend
            .migration_status(&commits, &committed_migrations)?
            .into_iter()
            .filter(|m| matches!(m.status, MigrationStatus::Committed { tried: _ }))
            .collect::<Vec<_>>();
        backend.execute_migrations(&to_be_executed)?;
    }
    Ok(())
}
