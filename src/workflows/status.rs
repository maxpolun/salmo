use std::collections::HashMap;

use crate::{backend::MigrationStatus, salmo_contex::SalmoContext};

pub struct TryStatus {
    pub up_to_date: bool,
    pub id: String,
}

pub struct StatusSummary {
    pub already_migrated: Vec<String>,
    pub pending_migration: Vec<String>,
    pub tries: Vec<TryStatus>,
    pub untried: Vec<String>,
}

pub struct CommittedStatus {
    pub already_migrated: Vec<String>,
    pub pending_migration: Vec<String>,
}

pub fn get_status(context: SalmoContext) -> anyhow::Result<HashMap<String, StatusSummary>> {
    let commits = context.commits()?;
    let migrations_registry = context.migrations()?;
    let all_migrations = migrations_registry.db.into_values().collect::<Vec<_>>();
    context
        .environments
        .iter()
        .map(|e| {
            let mut backend = e.backend()?;
            let s = backend.migration_status(&commits, all_migrations.as_slice())?;

            let already_migrated = s
                .iter()
                .filter_map(|m| match m.status {
                    MigrationStatus::Executed => Some(m.migration.id.clone()),
                    _ => None,
                })
                .collect();

            let pending_migration = s
                .iter()
                .filter_map(|m| match m.status {
                    MigrationStatus::Committed { tried: _ } => Some(m.migration.id.clone()),
                    _ => None,
                })
                .collect();

            let untried = s
                .iter()
                .filter_map(|m| match m.status {
                    MigrationStatus::Untried => Some(m.migration.id.clone()),
                    _ => None,
                })
                .collect();

            let tries = s
                .iter()
                .filter_map(|m| match m.status {
                    MigrationStatus::Tried { up_to_date } => Some(TryStatus {
                        up_to_date,
                        id: m.migration.id.clone(),
                    }),
                    _ => None,
                })
                .collect();

            Ok((
                e.name.clone(),
                StatusSummary {
                    already_migrated,
                    pending_migration,
                    tries,
                    untried,
                },
            ))
        })
        .collect()
}
