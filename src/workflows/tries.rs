use std::collections::HashMap;

use crate::{
    backend::{MigrationStatus, MigrationWithStatus},
    salmo_contex::SalmoContext,
};

// TODO: handle partial success/failure
pub fn reset_all_tries(
    ctx: &SalmoContext,
) -> anyhow::Result<HashMap<String, Vec<MigrationWithStatus>>> {
    let migrations_registry = ctx.migrations()?;
    let all_migrations = migrations_registry.db.into_values().collect::<Vec<_>>();
    let commits = ctx.commits()?;
    let mut output = HashMap::new();
    for env in ctx.environments.iter() {
        let mut backend = env.backend()?;
        let status = backend.migration_status(&commits, &all_migrations)?;
        let to_undo = status
            .into_iter()
            .filter(|m| matches!(m.status, MigrationStatus::Tried { up_to_date: _ }))
            .collect::<Vec<_>>();
        backend.untry_migrations(&to_undo, true)?;
        output.insert(env.name.clone(), to_undo);
    }
    Ok(output)
}
