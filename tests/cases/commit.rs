use std::fs;

use crate::common::TestContext;

#[test]
fn it_can_commit_a_tried_migration() {
    let ctx = TestContext::new("users");
    ctx.commit(&["create_users"]);
    ctx.migrate();
    ctx.run(&["try", "users_modified_at"]).unwrap();
    ctx.run(&["commit", "users_modified_at"]).unwrap();
    let commit_file =
        String::from_utf8(fs::read(ctx.dir.path().join("migrations/committed.toml")).unwrap())
            .unwrap();
    assert!(commit_file.contains("users_modified_at"));
    assert!(ctx
        .db
        .conn()
        .query("SELECT modified_at FROM users;", &[])
        .is_ok())
}

#[test]
fn it_cannot_commit_an_untried_migration() {
    let ctx = TestContext::new("users");
    ctx.commit(&["create_users"]);
    ctx.migrate();
    ctx.run(&["commit", "users_modified_at"]).unwrap();
    let commit_file =
        String::from_utf8(fs::read(ctx.dir.path().join("migrations/committed.toml")).unwrap())
            .unwrap();
    assert!(!commit_file.contains("users_modified_at"))
}
