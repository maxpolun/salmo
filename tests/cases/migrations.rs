use crate::common::TestContext;

#[test]
fn it_will_run_migrations() {
    let ctx = TestContext::new("users");
    ctx.commit(&["create_users", "users_modified_at"]);
    ctx.run(&["migrate"]).unwrap();
    let mut client = ctx.db.conn();
    client.execute("SELECT * FROM users;", &[]).unwrap();
    client
        .execute("SELECT modified_at FROM users;", &[])
        .unwrap();
}

#[test]
fn it_will_run_only_committed_migrations() {
    let ctx = TestContext::new("users");
    ctx.commit(&["create_users"]);
    ctx.run(&["migrate"]).unwrap();
    let mut client = ctx.db.conn();
    client.execute("SELECT * FROM users;", &[]).unwrap();
    client
        .execute("SELECT modified_at FROM users;", &[])
        .unwrap_err();
}

#[test]
fn it_can_use_a_custom_schema_for_meta_info() {
    let ctx = TestContext::new("custom-schema");
    ctx.commit(&["create_users", "users_modified_at"]);
    ctx.run(&["migrate"]).unwrap();
    let mut client = ctx.db.conn();
    client
        .execute(
            "SELECT * FROM custom_schema.salmo_executed_migrations;",
            &[],
        )
        .unwrap();
}
