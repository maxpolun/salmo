use std::fs;

use crate::common::TestContext;

#[test]
fn it_can_try_new_migrations() {
    let ctx = TestContext::new("users");
    ctx.commit(&["create_users"]);
    ctx.migrate();
    ctx.run(&["try", "users_modified_at"]).unwrap();
    let mut client = ctx.db.conn();
    client
        .execute("SELECT modified_at FROM users;", &[])
        .unwrap();
}

#[test]
fn it_can_untry_new_migrations() {
    let ctx = TestContext::new("users");
    ctx.commit(&["create_users"]);
    ctx.migrate();
    ctx.run(&["try", "users_modified_at"]).unwrap();
    ctx.run(&["untry", "users_modified_at"]).unwrap();
    let mut client = ctx.db.conn();
    client
        .execute("SELECT modified_at FROM users;", &[])
        .unwrap_err();
}

#[test]
fn it_can_retry_new_migrations() {
    let ctx = TestContext::new("users");
    ctx.commit(&["create_users"]);
    ctx.migrate();
    ctx.run(&["try", "users_modified_at"]).unwrap();
    fs::write(
        ctx.dir
            .path()
            .join("migrations/users_modified_at/migrate.sql"),
        "ALTER TABLE users ADD COLUMN modified_at_different TIMESTAMP WITH TIME ZONE;
    ",
    )
    .unwrap();

    ctx.run(&["retry", "users_modified_at"]).unwrap();
    let mut client = ctx.db.conn();
    client
        .execute("SELECT modified_at_different FROM users;", &[])
        .unwrap();
    client
        .execute("SELECT modified_at FROM users;", &[])
        .unwrap_err();
}
